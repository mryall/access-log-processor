### Access log processor

Processes Apache-format log files and extracts columns. Quickly.

#### Usage

Basic usage:

    java -jar processor.jar /path/to/file.log combined '%{bytes} %{verb} => %{request}'

That will print out something like this:

    4782 GET => /foo.html
    39573 POST => /save.cgi
    ...

Run the JAR with no arguments to see usage information, including supported formats and output fields.

    java -jar processor.jar

#### Supported log file formats

It currently supports three hard-coded formats for log input:

* Apache `common` format: `LogFormat "%h %l %u %t \"%r\" %>s %b" common`
* Apache `combined` format: `LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined`
* A non-standard `combined-timed` format: `LogFormat "%h %l %u %t \"%r\" %>s %b %D \"%{Referer}i\" \"%{User-Agent}i\"" combined-timed`

The code is pretty easy to adapt to support other formats. See RecordParsers.java for how the mapping is set up.

#### Building

Use Maven to build it. This has no dependencies other than Java 7 or later.

IDEA project files are included in the repo for your convenience, but some of the settings might need tweaking after opening.

#### Contributing

Pull requests welcome. Let's keep this lean and fast - no additional dependencies, please.

