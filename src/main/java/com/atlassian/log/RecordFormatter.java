package com.atlassian.log;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Formats a record, using format patterns in the format "some text %{variable} some text".
 */
public class RecordFormatter {
    public enum CalculatedField {
        /** Unix timestamp */
        UNIX_DATE,
        /** ISO-8601 formatted date in UTC */
        DATE,
        /** URL minus the query string */
        PATH
    }

    private static final Pattern VARIABLE_FORMAT = Pattern.compile("%\\{(.*?)\\}");

    private static FormatString[] parse(String format) {
        List<FormatString> formatStrings = new ArrayList<FormatString>();
        int lastIndex = 0;
        Matcher matcher = VARIABLE_FORMAT.matcher(format);
        while (matcher.find()) {
            if (matcher.start() > lastIndex) {
                formatStrings.add(new FixedString(format.substring(lastIndex, matcher.start())));
            }

            formatStrings.add(getFormatString(matcher.group(1)));
            lastIndex = matcher.end();
        }
        formatStrings.add(new FixedString(format.substring(lastIndex)));
        return formatStrings.toArray(new FormatString[formatStrings.size()]);
    }

    private static FormatString getFormatString(String variableName) {
        try {
            Field field = Field.valueOf(variableName.toUpperCase());
            return new FieldString(field);
        }
        catch (IllegalArgumentException e) {
            // not a direct field, fall through to try calculated fields
        }
        if (CalculatedField.UNIX_DATE.toString().equalsIgnoreCase(variableName)) {
            return new UnixDateString();
        }
        if (CalculatedField.DATE.toString().equalsIgnoreCase(variableName)) {
            return new IsoDateString();
        }
        if (CalculatedField.PATH.toString().equalsIgnoreCase(variableName)) {
            return new PathString();
        }
        throw new IllegalArgumentException("Unknown variable: " + variableName);
    }

    private final FormatString[] formatStrings;

    public RecordFormatter(String format) {
        this.formatStrings = parse(format);
    }

    public void append(Appendable appendable, Record record) throws IOException {
        for (FormatString formatString : formatStrings) {
            formatString.append(appendable, record);
        }
        appendable.append("\n");
    }

    private static interface FormatString {
        void append(Appendable appendable, Record record) throws IOException;
    }

    private static class FixedString implements FormatString {
        private final String value;

        private FixedString(String value) {
            this.value = value;
        }

        @Override
        public void append(Appendable appendable, Record record) throws IOException {
            appendable.append(value);
        }
    }

    private static class FieldString implements FormatString {
        private Field field;

        private FieldString(Field field) {
            this.field = field;
        }

        @Override
        public void append(Appendable appendable, Record record) throws IOException {
            appendable.append(record.getField(field));
        }
    }

    private static abstract class DateString implements FormatString {
        private static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("d/MMM/yyyy:HH:mm:ss Z", Locale.US);

        private String lastTimestamp;
        private String lastFormattedDate;

        @Override
        public final void append(Appendable appendable, Record record) throws IOException {
            String value = record.getField(Field.TIMESTAMP);
            if (value.equals(lastTimestamp)) { // repeated timestamps are cached
                appendable.append(lastFormattedDate);
                return;
            }
            try {
                Date date = TIMESTAMP_FORMAT.parse(value);
                lastTimestamp = value;
                lastFormattedDate = formatDate(date);
                appendable.append(lastFormattedDate);
            } catch (ParseException e) {
                appendable.append(defaultValue());
            }
        }

        abstract protected String defaultValue();

        abstract protected String formatDate(Date date);
    }

    private static class UnixDateString extends DateString {
        @Override
        protected String formatDate(Date date) {
            return String.valueOf(date.getTime() / 1000);
        }

        protected String defaultValue() {
            return "-1";
        }
    }

    private static class IsoDateString extends DateString {
        private static final SimpleDateFormat ISO_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        static {
            ISO_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        @Override
        protected String formatDate(Date date) {
            return ISO_DATE_FORMAT.format(date);
        }

        @Override
        protected String defaultValue() {
            return "0000-00-00 00:00:00";
        }
    }

    private static class PathString implements FormatString {
        private static final Pattern PATH_PATTERN = Pattern.compile("^(.*?)\\?.*$");

        // Atlassian-specific static resource prefix
        private static final Pattern STATIC_PREFIX = Pattern.compile("/s/(.*?)/_");

        @Override
        public void append(Appendable appendable, Record record) throws IOException {
            String url = record.getField(Field.REQUEST);
            String underlyingUrl = STATIC_PREFIX.matcher(url).replaceFirst("/s/~/_");
            Matcher pathMatcher = PATH_PATTERN.matcher(underlyingUrl);
            if (pathMatcher.matches()) {
                appendable.append(pathMatcher.group(1));
            }
            else {
                appendable.append(underlyingUrl);
            }
        }
    }
}
