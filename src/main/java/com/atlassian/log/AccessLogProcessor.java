package com.atlassian.log;

import java.io.*;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.log.RecordFormatter.CalculatedField;

public class AccessLogProcessor {
    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            printUsageInformation(System.err);
            System.exit(1);
        }

        String filename = args[0];
        RecordParser parser = RecordParsers.fromName(args[1]);
        RecordFormatter formatter = new RecordFormatter(args[2]);

        long startMillis = System.currentTimeMillis();
        AccessLogProcessor processor = new AccessLogProcessor(filename, parser);
        processor.process(formatter);
        System.err.println(String.format("Took %d ms to process %d records",
            System.currentTimeMillis() - startMillis, processor.getRecordsProcessed()));
    }

    private static void printUsageInformation(PrintStream writer) {
        writer.println("Usage: java -jar processor.jar FILE IN-FORMAT OUT-FORMAT");
        writer.println();
        writer.println("Input formats: common, combined, combined-timed, enhanced");
        writer.print("Output fields (direct): ");
        printEnumList(writer, "%s", Field.class);
        writer.print("Output field (calculated): ");
        printEnumList(writer, "%s", CalculatedField.class);
        writer.println();
        writer.println("Example: java -jar processor.jar access.log combined '%{bytes} %{request} [%{timestamp}]'");
    }

    private static void printEnumList(PrintStream writer, String format, Class<? extends Enum> enumType) {
        Enum[] values = enumType.getEnumConstants();
        for (int i = 0; i < values.length; i++) {
            writer.print(String.format(format, values[i].toString().toLowerCase()));
            if (i < values.length - 1) writer.print(", ");
        }
        writer.println();
    }

    private final LineNumberReader reader;
    private final RecordParser parser;
    private final AtomicLong recordsProcessed = new AtomicLong();


    public AccessLogProcessor(String filename, RecordParser parser) throws FileNotFoundException {
        this.parser = parser;
        this.reader = new LineNumberReader(new FileReader(filename));
    }

    private void process(RecordFormatter formatter) throws IOException {
        String line;
        while ((line = reader.readLine()) != null) {
            Record record;
            try {
                record = parser.parse(line);
            } catch (MalformedRecordException e) {
                System.err.println("Skipping malformed line " + reader.getLineNumber());
                continue;
            }

            formatter.append(System.out, record);
            recordsProcessed.incrementAndGet();
        }
    }

    private long getRecordsProcessed() {
        return recordsProcessed.get();
    }
}
