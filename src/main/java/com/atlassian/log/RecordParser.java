package com.atlassian.log;

import java.util.EnumMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecordParser {
    private final Pattern pattern;
    private final EnumMap<Field, Integer> fieldToGroupMapping;

    public RecordParser(Pattern pattern, EnumMap<Field, Integer> fieldToGroupMapping) {
        this.pattern = pattern;
        this.fieldToGroupMapping = fieldToGroupMapping;
    }

    public Record parse(String line) throws MalformedRecordException {
        Matcher matcher = pattern.matcher(line);
        if (!matcher.matches()) {
            throw new MalformedRecordException(line);
        }
        return new Record(fieldToGroupMapping, matcher);
    }
}
