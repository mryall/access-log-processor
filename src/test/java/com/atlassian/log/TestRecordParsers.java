package com.atlassian.log;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestRecordParsers {
    @Test
    public void testCombinedTimed() throws Exception {
        String line = "131.103.27.179 - - [14/Mar/2014:00:00:02 +1100] \"GET /wiki/rest/capabilities HTTP/1.0\" 200 910 4207 \"-\" \"JIRA-6.3-ClusteringEAP03 (6304)\"";
        RecordParser parser = RecordParsers.COMBINED_TIMED;
        Record record = parser.parse(line);
        assertEquals(record.getField(Field.CLIENT_IP), "131.103.27.179");
        assertEquals(record.getField(Field.IDENT), "-");
        assertEquals(record.getField(Field.AUTH), "-");
        assertEquals(record.getField(Field.TIMESTAMP), "14/Mar/2014:00:00:02 +1100");
        assertEquals(record.getField(Field.VERB), "GET");
        assertEquals(record.getField(Field.REQUEST), "/wiki/rest/capabilities");
        assertEquals(record.getField(Field.HTTP_VERSION), "HTTP/1.0");
        assertEquals(record.getField(Field.RESPONSE), "200");
        assertEquals(record.getField(Field.BYTES), "910");
        assertEquals(record.getField(Field.DURATION), "4207");
        assertEquals(record.getField(Field.REFERER), "-");
        assertEquals(record.getField(Field.AGENT), "JIRA-6.3-ClusteringEAP03 (6304)");
    }

    @Test
    public void testCombinedTimedInvalid() throws Exception {
        String line = "131.103.27.179 - - [14/Mar/2014:00:00:02 +1100] \"GET /wiki/rest/capabilities HTTP/1.0\" 200 910 \"-\" \"JIRA-6.3-ClusteringEAP03 (6304)\"";
        RecordParser parser = RecordParsers.COMBINED_TIMED;
        try {
            parser.parse(line);
            fail("Expected exception not thrown");
        } catch (MalformedRecordException expected) {
            // expected flow
        }
    }

    @Test
    public void testCombined() throws Exception {
        String line = "131.103.27.179 - - [14/Mar/2014:00:00:02 +1100] \"GET /wiki/rest/capabilities HTTP/1.0\" 200 910 \"-\" \"JIRA-6.3-ClusteringEAP03 (6304)\"";
        RecordParser parser = RecordParsers.COMBINED;
        Record record = parser.parse(line);
        assertEquals(record.getField(Field.CLIENT_IP), "131.103.27.179");
        assertEquals(record.getField(Field.IDENT), "-");
        assertEquals(record.getField(Field.AUTH), "-");
        assertEquals(record.getField(Field.TIMESTAMP), "14/Mar/2014:00:00:02 +1100");
        assertEquals(record.getField(Field.VERB), "GET");
        assertEquals(record.getField(Field.REQUEST), "/wiki/rest/capabilities");
        assertEquals(record.getField(Field.HTTP_VERSION), "HTTP/1.0");
        assertEquals(record.getField(Field.RESPONSE), "200");
        assertEquals(record.getField(Field.BYTES), "910");
        assertEquals(record.getField(Field.DURATION), "");
        assertEquals(record.getField(Field.REFERER), "-");
        assertEquals(record.getField(Field.AGENT), "JIRA-6.3-ClusteringEAP03 (6304)");
    }
}
