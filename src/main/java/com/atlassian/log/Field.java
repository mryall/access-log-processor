package com.atlassian.log;

enum Field {
    CLIENT_IP,
    IDENT,
    AUTH,
    TIMESTAMP,
    VERB,
    REQUEST,
    HTTP_VERSION,
    RESPONSE,
    BYTES,
    DURATION,
    REFERER,
    AGENT,
    ACCEPT,
    ACCEPT_CHARSET,
    ACCEPT_ENCODING,
    CACHE_CONTROL,
    CONTENT_TYPE,
    PRAGMA,
    X_AUSERNAME
}
