package com.atlassian.log;

import java.util.EnumMap;
import java.util.regex.Pattern;

public class RecordParsers {
    private RecordParsers() { // prevent instantiation
    }

    public static RecordParser fromName(String name) throws IllegalArgumentException {
        if ("combined-timed".equalsIgnoreCase(name)) return COMBINED_TIMED;
        if ("combined".equalsIgnoreCase(name)) return COMBINED;
        if ("common".equalsIgnoreCase(name)) return COMMON;
        if ("enhanced".equalsIgnoreCase(name)) return ENHANCED;
        throw new IllegalArgumentException("Unknown parser: " + name);
    }

    /**
     * A custom Apache access log format, 'combined' format with duration ("%D") inserted at position 10.
     *
     * LogFormat "%h %l %u %t \"%r\" %>s %b %D \"%{Referer}i\" \"%{User-Agent}i\"" combined-timed
     */
    public static final RecordParser COMBINED_TIMED;

    static {
        // 131.103.27.179 - - [14/Mar/2014:00:00:02 +1100] "GET /wiki/rest/capabilities HTTP/1.0" 200 910 4207 "-" "JIRA-6.3-ClusteringEAP03 (6304)"
        final Pattern pattern = Pattern.compile(
            "^(\\S+) (\\S+) (\\S+) \\[(.*?)\\] \"(\\S+) (\\S+) (\\S+)\" (\\S+) (\\S+) (\\S+) \"(.*?)\" \"(.*?)\"$");

        final EnumMap<Field, Integer> fieldToGroupMapping = new EnumMap<Field, Integer>(Field.class);
        fieldToGroupMapping.put(Field.CLIENT_IP, 1); // field -> matcher group
        fieldToGroupMapping.put(Field.IDENT, 2);
        fieldToGroupMapping.put(Field.AUTH, 3);
        fieldToGroupMapping.put(Field.TIMESTAMP, 4);
        fieldToGroupMapping.put(Field.VERB, 5);
        fieldToGroupMapping.put(Field.REQUEST, 6);
        fieldToGroupMapping.put(Field.HTTP_VERSION, 7);
        fieldToGroupMapping.put(Field.RESPONSE, 8);
        fieldToGroupMapping.put(Field.BYTES, 9);
        fieldToGroupMapping.put(Field.DURATION, 10);
        fieldToGroupMapping.put(Field.REFERER, 11);
        fieldToGroupMapping.put(Field.AGENT, 12);

        COMBINED_TIMED = new RecordParser(pattern, fieldToGroupMapping);
    }


    /**
     * Apache 'combined' format.
     *
     * LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
     */
    public static final RecordParser COMBINED;

    static {
        // 131.103.27.179 - - [14/Mar/2014:00:00:02 +1100] "GET /wiki/rest/capabilities HTTP/1.0" 200 910 "-" "JIRA-6.3-ClusteringEAP03 (6304)"
        final Pattern pattern = Pattern.compile(
            "^(\\S+) (\\S+) (\\S+) \\[(.*?)\\] \"(\\S+) (\\S+) (\\S+)\" (\\S+) (\\S+) \"(.*?)\" \"(.*?)\"$");

        final EnumMap<Field, Integer> fieldToGroupMapping = new EnumMap<Field, Integer>(Field.class);
        fieldToGroupMapping.put(Field.CLIENT_IP, 1);
        fieldToGroupMapping.put(Field.IDENT, 2);
        fieldToGroupMapping.put(Field.AUTH, 3);
        fieldToGroupMapping.put(Field.TIMESTAMP, 4);
        fieldToGroupMapping.put(Field.VERB, 5);
        fieldToGroupMapping.put(Field.REQUEST, 6);
        fieldToGroupMapping.put(Field.HTTP_VERSION, 7);
        fieldToGroupMapping.put(Field.RESPONSE, 8);
        fieldToGroupMapping.put(Field.BYTES, 9);
        fieldToGroupMapping.put(Field.REFERER, 10);
        fieldToGroupMapping.put(Field.AGENT, 11);

        COMBINED = new RecordParser(pattern, fieldToGroupMapping);
    }

    /**
     * Apache 'common' format.
     * <p/>
     * <code>LogFormat "%h %l %u %t \"%r\" %>s %b" common</code>
     */
    public static final RecordParser COMMON;

    static {
        // 131.103.27.179 - - [14/Mar/2014:00:00:02 +1100] "GET /wiki/rest/capabilities HTTP/1.0" 200 910
        final Pattern pattern = Pattern.compile(
            "^(\\S+) (\\S+) (\\S+) \\[(.*?)\\] \"(\\S+) (\\S+) (\\S+)\" (\\S+) (\\S+)$");

        final EnumMap<Field, Integer> fieldToGroupMapping = new EnumMap<Field, Integer>(Field.class);
        fieldToGroupMapping.put(Field.CLIENT_IP, 1);
        fieldToGroupMapping.put(Field.IDENT, 2);
        fieldToGroupMapping.put(Field.AUTH, 3);
        fieldToGroupMapping.put(Field.TIMESTAMP, 4);
        fieldToGroupMapping.put(Field.VERB, 5);
        fieldToGroupMapping.put(Field.REQUEST, 6);
        fieldToGroupMapping.put(Field.HTTP_VERSION, 7);
        fieldToGroupMapping.put(Field.RESPONSE, 8);
        fieldToGroupMapping.put(Field.BYTES, 9);

        COMMON = new RecordParser(pattern, fieldToGroupMapping);
    }

    /**
     * Atlassian 'enhanced' format, configured in nginx as follows:
     * <p/>
     * <pre>log_format enhanced '$remote_addr - $remote_user [$time_local] '
     *      '"$request" $status $body_bytes_sent '
     *      '$request_time "$http_referer" "$http_user_agent" '
     *      '"$http_accept" "$http_accept_charset" '
     *      '"$http_accept_encoding" "$http_cache_control" '
     *      '"$http_content_type" "$http_pragma" "$sent_http_x_ausername"';</pre>
     * <p/>
     * It's used for replayable log files.
     */
    public static final RecordParser ENHANCED;

    static {
        // 172.24.12.63 - - [25/Jun/2014:06:17:00 +0000] "GET /rest/helptips/1.0/tips HTTP/1.1" 200 67 0.035 "https://extranet.atlassian.com/display/PM/Recommendation%3A+Confluence+Tasks+and+JIRA+issues" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36" "application/json, text/javascript, */*; q=0.01" "-" "gzip,deflate,sdch" "max-age=0" "application/json" "-" "nprasad"
        final Pattern pattern = Pattern.compile(
            "^(\\S+) (\\S+) (\\S+) \\[(.*?)\\] \"(\\S+) (\\S+) (\\S+)\" (\\S+) (\\S+) (\\S+)" + // everything up to duration
            " \"(.*?)\" \"(.*?)\"" + // Referrer, UAz
            " \"(.*?)\" \"(.*?)\"" + // Accept, Accept-Charset
            " \"(.*?)\" \"(.*?)\"" + // Accept-Encoding, Cache-Control
            " \"(.*?)\" \"(.*?)\"" + // Content-Type, Pragma
            " \"(.*?)\"$"); // Username

        final EnumMap<Field, Integer> fieldToGroupMapping = new EnumMap<Field, Integer>(Field.class);
        fieldToGroupMapping.put(Field.CLIENT_IP, 1);
        fieldToGroupMapping.put(Field.IDENT, 2);
        fieldToGroupMapping.put(Field.AUTH, 3);
        fieldToGroupMapping.put(Field.TIMESTAMP, 4);
        fieldToGroupMapping.put(Field.VERB, 5);
        fieldToGroupMapping.put(Field.REQUEST, 6);
        fieldToGroupMapping.put(Field.HTTP_VERSION, 7);
        fieldToGroupMapping.put(Field.RESPONSE, 8);
        fieldToGroupMapping.put(Field.BYTES, 9);
        fieldToGroupMapping.put(Field.DURATION, 10);
        fieldToGroupMapping.put(Field.REFERER, 11);
        fieldToGroupMapping.put(Field.AGENT, 12);
        fieldToGroupMapping.put(Field.ACCEPT, 13);
        fieldToGroupMapping.put(Field.ACCEPT_CHARSET, 14);
        fieldToGroupMapping.put(Field.ACCEPT_ENCODING, 15);
        fieldToGroupMapping.put(Field.CACHE_CONTROL, 16);
        fieldToGroupMapping.put(Field.CONTENT_TYPE, 17);
        fieldToGroupMapping.put(Field.PRAGMA, 18);
        fieldToGroupMapping.put(Field.X_AUSERNAME, 19);

        ENHANCED = new RecordParser(pattern, fieldToGroupMapping);
    }

}
