package com.atlassian.log;

import java.util.EnumMap;
import java.util.regex.MatchResult;

/** Backed by a MatchResult so we don't need to construct any additional objects when processing a record. */
public class Record {
    private final EnumMap<Field, Integer> fieldToGroupMapping;
    private final MatchResult matchResult;

    public Record(EnumMap<Field, Integer> fieldToGroupMapping, MatchResult matchResult) {
        this.fieldToGroupMapping = fieldToGroupMapping;
        this.matchResult = matchResult;
    }

    /** Return the value associated with the field, or {@literal ""} if that field doesn't exist in this record. */
    public String getField(Field field) {
        if (!fieldToGroupMapping.containsKey(field)) return "";
        try {
            return matchResult.group(fieldToGroupMapping.get(field));
        } catch (IndexOutOfBoundsException e) {
            return "";
        }
    }
}
