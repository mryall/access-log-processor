package com.atlassian.log;

public class MalformedRecordException extends Exception {
    public MalformedRecordException(String message) {
        super(message);
    }

    public MalformedRecordException(String message, Throwable cause) {
        super(message, cause);
    }

    public MalformedRecordException(Throwable cause) {
        super(cause);
    }

    public MalformedRecordException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
