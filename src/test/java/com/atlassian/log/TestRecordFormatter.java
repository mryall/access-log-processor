package com.atlassian.log;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestRecordFormatter {
    private Record record;

    @Before
    public void setUp() throws Exception {
        String line = "131.103.27.179 - - [14/Mar/2014:00:00:02 +1100] \"GET /wiki/rest/capabilities HTTP/1.0\" 200 910 \"-\" \"JIRA-6.3-ClusteringEAP03 (6304)\"";
        record = RecordParsers.COMBINED.parse(line);
    }


    @Test
    public void testSimpleFormat() throws Exception {
        RecordFormatter formatter = new RecordFormatter("%{bytes} %{request} [%{timestamp}]");
        StringBuilder result = new StringBuilder();
        formatter.append(result, record);
        assertEquals("910 /wiki/rest/capabilities [14/Mar/2014:00:00:02 +1100]\n", result.toString());
    }
}
